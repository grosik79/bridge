package pl.sda.vehicle;

public class Aircraft implements IVehicle {
    private String aircraft;

    public Aircraft(String aircraft) {
        this.aircraft = aircraft;
    }

    @Override
    public void moveRight() {
        System.out.println(" Aircraft " + aircraft + " move right");
    }

    @Override
    public void moveLeft() {
        System.out.println(" Aircraft " + aircraft + " move left");
    }

    @Override
    public void moveUp() {
        System.out.println(" Aircraft " + aircraft + " move up");
    }

    @Override
    public void moveDown() {
        System.out.println(" Aircraft " + aircraft + " move down");
    }
}
