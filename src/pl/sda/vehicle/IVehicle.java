package pl.sda.vehicle;

public interface IVehicle {
    void moveRight();
    void moveLeft();
    void moveUp();
    void moveDown();

}
