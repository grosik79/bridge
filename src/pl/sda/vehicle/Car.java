package pl.sda.vehicle;

public class Car implements IVehicle {
private String car;

    public Car(String car) {
        this.car = car;
    }

    @Override
    public void moveRight() {
        System.out.println(" Car " + car + " move right ");
    }

    @Override
    public void moveLeft() {
        System.out.println(" Car " + car + " move left ");

    }

    @Override
    public void moveUp() {
        System.out.println(" Car " + car + " move up");
    }

    @Override
    public void moveDown() {
        System.out.println(" Car " + car + " move down");
    }
}
