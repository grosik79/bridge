package pl.sda.vehicle;

public class Boat implements IVehicle {

    private String boat;

    public Boat(String boat) {
        this.boat = boat;
    }

    @Override
    public void moveRight() {
        System.out.println(" Boat" + boat + " move right");
    }

    @Override
    public void moveLeft() {
        System.out.println(" Boat" + boat + " move left");
    }

    @Override
    public void moveUp() {
        System.out.println(" Boat" + boat + " move up");

    }

    @Override
    public void moveDown() {
        System.out.println(" Boat" + boat + " move down");
    }
}
