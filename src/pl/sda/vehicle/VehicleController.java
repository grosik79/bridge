package pl.sda.vehicle;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class VehicleController {
    static List<IVehicle> vehicles = new ArrayList<IVehicle>();

    public static void main(String[] args) {
        vehicles.add(new Car("Porsche"));
        vehicles.add(new Boat("Kaja"));
        vehicles.add(new Car("Audi"));
        vehicles.add(new Car("Serenka"));
        vehicles.add(new Aircraft("Beoaning"));
        vehicles.add(new Aircraft("AirBus"));
        vehicles.add(new Boat("ZawiszaCzarny"));

        while (true){
            System.out.println(" Podaj cyfrę od 1-4 ");

            Scanner sc = new Scanner(System.in);
            int x = sc.nextInt();
            if (x == 1){
                vehicles.forEach(y->y.moveUp());
            }else if(x == 2){
                vehicles.forEach(y->y.moveDown());
            }else if(x==3){
                vehicles.forEach(y->y.moveLeft());
            }else if(x==4){
                vehicles.forEach(y->y.moveRight());
            }
        }



    }

}
