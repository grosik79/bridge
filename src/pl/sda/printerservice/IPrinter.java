package pl.sda.printerservice;

import java.io.FileNotFoundException;

public interface IPrinter {
    void printMessage(String message) throws FileNotFoundException;
}
